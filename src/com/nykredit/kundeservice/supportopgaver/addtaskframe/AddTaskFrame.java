package com.nykredit.kundeservice.supportopgaver.addtaskframe;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import com.nykredit.kundeservice.swing.Calendar;
import com.nykredit.kundeservice.swing.NDialog;
import com.nykredit.kundeservice.supportopgaver.customarraylistobjects.Task;
import com.nykredit.kundeservice.supportopgaver.database.SOracle;
import com.nykredit.kundeservice.supportopgaver.tasksolveframe.TaskSolveFrame;
import com.nykredit.kundeservice.swing.IntTextField;


/**
 * 
 * @author crpe
 * @description In this frame users can add tasks that needs to be solved
 */
public class AddTaskFrame extends NDialog {

	private static final long serialVersionUID = 1L;
	JLabel dateLabel;
	JComboBox<Task> taskListComboBox = new JComboBox<Task>();;
	SOracle sOracle;
	IntTextField noTask = new IntTextField(4);
	TaskSolveFrame parent;
	int selectedTaskIndex;

	public AddTaskFrame(SOracle sOracle, TaskSolveFrame parent,int selectedTaskIndex){
		this.selectedTaskIndex = selectedTaskIndex;
		this.parent = parent;
		this.sOracle = sOracle;
		this.setTitle("Tilf�j Opgave(r)");
		this.setContentPane(getMainPane());
		this.pack();
		this.setResizable(false);
		this.setModal(true);
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
	}
	
	private JPanel getMainPane(){
		JPanel p = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		c.insets = new Insets(10,10,0,0);
		c.anchor = GridBagConstraints.NORTHWEST;
		c.gridx = 0;
		c.gridy = 0;
		p.add(new JLabel(" Modtaget",JLabel.LEFT),c);
		c.insets = new Insets(10,10,0,0);
		c.gridx = 1;
		c.gridy = 0;		
		p.add(new JLabel(" V�lg Opgave",JLabel.LEFT),c);
		//c.anchor = GridBagConstraints.SOUTHEAST;
		c.insets = new Insets(10,10,0,0);
		c.gridx = 2;
		c.gridy = 0;
		p.add(new JLabel(" Antal",JLabel.LEFT),c);
		
		
		c.insets = new Insets(0,10,10,0);
		//c.anchor = GridBagConstraints.NORTHWEST;
		c.gridx = 0;
		c.gridy = 1;
		p.add(getDateTextField(),c);
		c.insets = new Insets(0,10,10,0);
		c.gridx = 1;
		c.gridy = 1;		
		p.add(getTaskListComboBox(),c);
		//c.anchor = GridBagConstraints.SOUTHEAST;
		c.insets = new Insets(0,10,10,0);
		c.gridx = 2;
		c.gridy = 1;
		p.add(getNoTask(),c);
		c.insets = new Insets(0,10,10,10);
		c.gridx = 3;
		c.gridy = 1;
		p.add(getAddButton(),c);
		return p;
	}
	
	private JLabel getDateTextField(){
		dateLabel = new JLabel(now(),JLabel.CENTER);
		dateLabel.setPreferredSize(new Dimension(70,20));
		dateLabel.setBackground(new Color(255,255,255));
		dateLabel.setOpaque(true);
		dateLabel.setBorder(noTask.getBorder());
		dateLabel.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				Calendar cal = new Calendar(dateLabel.getText()+" 00:00:00:000");
				dateLabel.setText(cal.getOracleDate());
			}
		});
		return dateLabel;
	}
	
	private JComboBox<Task> getTaskListComboBox(){
		taskListComboBox.setPreferredSize(new Dimension(200,20));
		ArrayList<Task> tl = sOracle.getTaskList();
		for(Task task:tl){
			taskListComboBox.addItem(task);
		}
		taskListComboBox.setSelectedIndex(selectedTaskIndex);
		return taskListComboBox;
	}
	
	private IntTextField getNoTask(){
		noTask.setPreferredSize(new Dimension(40,20));
		noTask.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {
			    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			    	addTasks();
		        }
			}
			public void keyReleased(KeyEvent e) {}
			public void keyTyped(KeyEvent e) {}
		});
		return noTask;
	}
	
	private JButton getAddButton(){
		JButton b = new JButton("Tilf�j");
		b.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				addTasks();
			}
		});
		b.addKeyListener(new KeyListener() {
		public void keyPressed(KeyEvent e) {
		    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
		    	addTasks();
	        }
		}
		public void keyReleased(KeyEvent e) {}
		public void keyTyped(KeyEvent e) {}
		});
		return b;
	}
	
	private void addTasks(){
		if (validateDate()){

		String deadline = sOracle.getDeadline(getReceivedDate(), getDeadline());
		
		if (!deadline.contentEquals("ERROR")){
		
		sOracle.addTask(getNumberOfTasks(),getTaskID(),getAgent(),getReceivedDate(),deadline);
		JOptionPane.showMessageDialog(AddTaskFrame.this,
				getNumberOfTasks() + " Opgave(r) er nu blevet tilf�jet",
			    "Besked",
			    JOptionPane.INFORMATION_MESSAGE);
		noTask.setText("");
		parent.updatePanel();
		}else{
			JOptionPane.showMessageDialog(parent,
					"<html>Du kan ikke tilf�je flere opgaver,<br>f�r driften har opdateret KS_DRIFT.SYS_DATE_KS",
				    "System Fejl",
				    JOptionPane.ERROR_MESSAGE);
		}
		
		}
	}
	
	
	private int getNumberOfTasks(){
		try{
		return Integer.parseInt(noTask.getText());
		}catch(NumberFormatException e){return 0;}
	}

	private int getTaskID(){
		return ((Task)taskListComboBox.getSelectedItem()).getTaskID();
	}

	private int getDeadline(){
		return ((Task)taskListComboBox.getSelectedItem()).getDeadline();
	}
	
	private String getAgent(){
		return System.getenv("USERNAME").toUpperCase();
	}
	
	private String getReceivedDate(){
		return dateLabel.getText();
	}
	
	private String now() {
		java.util.Calendar cal = java.util.Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(cal.getTime());
	}

	private boolean validateDate(){
		boolean dateOK = true;
		DateFormat formatter; 
		java.util.Date date; 
		
		try {	
		  formatter = new SimpleDateFormat("yyyy-MM-dd");
		  date = (java.util.Date)formatter.parse(getReceivedDate());  
		  
		  java.util.Calendar cal = java.util.Calendar.getInstance();
		  if (cal.getTime().compareTo(date)<0){
			  
		  JOptionPane.showMessageDialog(AddTaskFrame.this,
					"Du kan ikke tilf�je opgaver ud i fremtiden",
				    "Fejl-40",
				    JOptionPane.ERROR_MESSAGE);
		  dateOK = false;
		  }
		} catch (ParseException e) {} 
		return dateOK;
	}

}
