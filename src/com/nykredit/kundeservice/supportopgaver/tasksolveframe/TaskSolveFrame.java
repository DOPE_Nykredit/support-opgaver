package com.nykredit.kundeservice.supportopgaver.tasksolveframe;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import com.nykredit.kundeservice.swing.NFrame;
import com.nykredit.kundeservice.supportopgaver.addtaskframe.AddTaskFrame;
import com.nykredit.kundeservice.supportopgaver.customarraylistobjects.Task;
import com.nykredit.kundeservice.supportopgaver.database.SOracle;
import com.nykredit.kundeservice.supportopgaver.overviewframe.Overview;
import com.nykredit.kundeservice.supportopgaver.taskadministrationframe.TaskAdministration;
import com.nykredit.kundeservice.supportopgaver.totalframe.TotalFrame;



/**
 * @author CRPE
 * @description
 * This class displays the frame in which the user submit the number of solved tasks
 */
public class TaskSolveFrame extends NFrame implements WindowListener {

	private static final long serialVersionUID = 1L;
	private JComboBox<Task> taskListComboBox = new JComboBox<Task>();
	private TaskPanelPanel taskPanelPanel;
	private SOracle sOracle;
	

	public TaskSolveFrame(String program){
		WindowListener[] test = this.getWindowListeners();
		for (WindowListener list : test) {
			this.removeWindowListener(list);
		}
		this.addWindowListener(this);
		sOracle = new SOracle(program);
		this.setContentPane(getMainPanel());
		this.setTitle(program+" - "+getAgent());
		this.pack();
		this.setLocationRelativeTo(getRootPane());
		this.setResizable(false);
		this.setVisible(true);

	}
	
	private JPanel getMainPanel(){
		JPanel p = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		

		c.insets = new Insets(5,5,0,5);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.NORTHWEST;
		p.add(getTaskListComboBox(),c);
		
		c.fill = GridBagConstraints.NONE;
		c.insets = new Insets(5,12,0,5);
		c.gridy = 1;		
		p.add(getTaskList(),c);

		c.insets = new Insets(5,5,0,5);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridy = 2;		
		p.add(getButtonPanel(),c);
		
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.EAST;
		c.gridy = 3;
		c.insets = new Insets(5,5,5,5);
		p.add(new JLabel("CRPE � Nykredit"),c);	
		
		return p;
	}
	
	
	private JPanel getButtonPanel(){
		JPanel p = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		p.add(getUpdateButton(),c);

		c.gridx = 1;
		p.add(getAddTaskFrame(),c);
		
		c.gridx = 0;
		c.gridy = 1;
		p.add(getOverviewFrame(),c);
		
		c.gridx = 1;
		p.add(getTaskAdministrationFrame(),c);
		
		c.gridx = 0;
		c.gridy = 2;
		p.add(getTotalFrame(),c);
		
		return p;
	}
	
	
	public void updatePanel(){
		taskPanelPanel.updateList();
		TaskSolveFrame.this.repaint();
		TaskSolveFrame.this.validate();
		TaskSolveFrame.this.pack();
	}
	
	private JComboBox<Task> getTaskListComboBox(){
		taskListComboBox.setPreferredSize(new Dimension(200,20));
		ArrayList<Task> tl = sOracle.getTaskList();
		for(Task task:tl){
			taskListComboBox.addItem(task);
		}
		taskListComboBox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				updatePanel();
			}
		});
		return taskListComboBox;
	}
	
	private TaskPanelPanel getTaskList(){
		taskPanelPanel = new TaskPanelPanel(sOracle,TaskSolveFrame.this);
		return taskPanelPanel;
	}
	
	private JButton getUpdateButton(){
		JButton b = new JButton("Opdat�r");
		b.setPreferredSize(new Dimension(150,20));
		b.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				updatePanel();
			}
		});
		b.addKeyListener(new KeyListener() {
		public void keyPressed(KeyEvent e) {
		    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
		    	updatePanel();
	        }
		}
		public void keyReleased(KeyEvent e) {}
		public void keyTyped(KeyEvent e) {}
		});
		return b;
	}

	private JButton getTaskAdministrationFrame(){
		JButton b = new JButton("Opgaveliste");
		b.setPreferredSize(new Dimension(150,20));
		b.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				new TaskAdministration(TaskSolveFrame.this,sOracle);
			}
		});
		b.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {
			    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			    	new TaskAdministration(TaskSolveFrame.this,sOracle);
		        }
			}
			public void keyReleased(KeyEvent e) {}
			public void keyTyped(KeyEvent e) {}
			});
		return b;
	}
	
	private JButton getAddTaskFrame(){
		JButton b = new JButton("Tilf�j Opgave(r)");
		b.setPreferredSize(new Dimension(150,20));
		b.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				new AddTaskFrame(sOracle,TaskSolveFrame.this,taskListComboBox.getSelectedIndex());
			}
		});
		b.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {
			    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			    	new AddTaskFrame(sOracle,TaskSolveFrame.this,taskListComboBox.getSelectedIndex());
		        }
			}
			public void keyReleased(KeyEvent e) {}
			public void keyTyped(KeyEvent e) {}
			});
		return b;
	}

	private JButton getOverviewFrame(){
		JButton b = new JButton("Oversigt (Tovholder)");
		b.setPreferredSize(new Dimension(150,20));
		b.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
		    	Overview overview = new Overview(sOracle,TaskSolveFrame.this);
		    	overview.setDefaultCloseOperation( WindowConstants.DO_NOTHING_ON_CLOSE );
			}
		});
		b.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {
			    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			    	Overview overview = new Overview(sOracle,TaskSolveFrame.this);
			    	overview.setDefaultCloseOperation( WindowConstants.DO_NOTHING_ON_CLOSE );
		        }
			}
			public void keyReleased(KeyEvent e) {}
			public void keyTyped(KeyEvent e) {}
			});
		return b;
	}
	
	private JButton getTotalFrame(){
		JButton b = new JButton("Total oversigt");
		b.setPreferredSize(new Dimension(150,20));
		b.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				TotalFrame totalFrame = new TotalFrame(sOracle,TaskSolveFrame.this);
		    	totalFrame.setDefaultCloseOperation( WindowConstants.DO_NOTHING_ON_CLOSE );
			}
		});
		b.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {
			    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			    	TotalFrame totalFrame = new TotalFrame(sOracle,TaskSolveFrame.this);
			    	totalFrame.setDefaultCloseOperation( WindowConstants.DO_NOTHING_ON_CLOSE );
		        }
			}
			public void keyReleased(KeyEvent e) {}
			public void keyTyped(KeyEvent e) {}
			});
		return b;
	}
	
	public String getAgent(){
		return System.getenv("USERNAME").toUpperCase();
	}
	
	public int getTaskID(){
		return ((Task)taskListComboBox.getSelectedItem()).getTaskID();
	}
	
	public void windowActivated(WindowEvent arg0) {}
	public void windowClosed(WindowEvent arg0) {}
	public void windowClosing(WindowEvent arg0) {this.dispose();}
	public void windowDeactivated(WindowEvent arg0) {}
	public void windowDeiconified(WindowEvent arg0) {}
	public void windowIconified(WindowEvent arg0) {}
	public void windowOpened(WindowEvent arg0) {}
	
}
