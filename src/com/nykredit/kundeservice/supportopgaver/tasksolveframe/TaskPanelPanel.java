package com.nykredit.kundeservice.supportopgaver.tasksolveframe;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.nykredit.kundeservice.supportopgaver.database.SOracle;


public class TaskPanelPanel extends JPanel{

	private static final long serialVersionUID = 1L;
	private SOracle sOracle;
	private TaskSolveFrame parent;


	public TaskPanelPanel(SOracle sOracle,TaskSolveFrame parent){
		this.sOracle = sOracle;
		this.parent = parent;
		this.setLayout(new GridBagLayout());
		updateList();
	}
	
	private JPanel getHeaderPanel(){
		JPanel p = new JPanel(new GridBagLayout());
		
		JLabel modtaget = new JLabel("Modtaget",JLabel.LEFT);
		modtaget.setPreferredSize(new Dimension(70,20));
		JLabel rest = new JLabel("Rest",JLabel.LEFT);
		rest.setPreferredSize(new Dimension(40,20));
		JLabel l�st = new JLabel("L�st",JLabel.LEFT);
		l�st.setPreferredSize(new Dimension(40,20));
		JLabel l�stdato = new JLabel("L�st dato",JLabel.LEFT);
		l�stdato.setPreferredSize(new Dimension(70,20));
		
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(0,0,5,0);
		c.gridx = 0;
		c.gridy = 0;
		p.add(modtaget,c);
		c.insets = new Insets(0,5,5,0);
		c.gridx = 1;
		c.gridy = 0;
		p.add(rest,c);
		c.insets = new Insets(0,5,5,0);
		c.gridx = 2;
		c.gridy = 0;
		p.add(l�st,c);	
		c.insets = new Insets(0,5,5,0);
		c.gridx = 3;
		c.gridy = 0;
		p.add(l�stdato,c);	
		return p;
	}
	

	public void updateList(){
		this.removeAll();
		GridBagConstraints c = new GridBagConstraints();
		
		ResultSet rs = sOracle.getTaskList(parent.getTaskID());
		
		int y = 0;
		
		c.anchor = GridBagConstraints.WEST;
		c.gridy = y;
		c.gridx = 0;
		c.insets = new Insets(0,0,0,0);
		this.add(getHeaderPanel(),c);
		c.fill = GridBagConstraints.HORIZONTAL;
		try {
			while(rs.next()){
				y++;
				c.gridy = y;
				this.add(new TaskPanel(rs.getString("RECEIVED_DATE"), rs.getString("REMAING_TASKS"), this, sOracle),c);
			}
		} catch (SQLException e) {e.printStackTrace();}
		
	}
	
	public int getTaskID(){
		return parent.getTaskID();
	}
	
	public void updateTaskPanelPanel(){
		parent.updatePanel();
	}

}
