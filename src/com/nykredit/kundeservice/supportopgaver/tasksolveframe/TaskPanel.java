package com.nykredit.kundeservice.supportopgaver.tasksolveframe;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import com.nykredit.kundeservice.swing.Calendar;
import com.nykredit.kundeservice.supportopgaver.database.SOracle;
import com.nykredit.kundeservice.swing.IntTextField;


public class TaskPanel extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel labelReceivedDateLabel;
	private JLabel labelRemainingTasks;
	private JLabel labelSolvedDate;
	private IntTextField intTextFieldsolved = new IntTextField(4);
	private TaskPanelPanel parent;
	private SOracle sOracle;
	
	

	public TaskPanel(String receivedDate,String remainingTasks,TaskPanelPanel parent,SOracle sOracle){
		this.parent = parent;
		this.sOracle = sOracle;
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.WEST;
		c.insets = new Insets(0,0,5,0);
		c.gridx = 0;
		c.gridy = 0;
		this.add(getLabelReceivedDate(receivedDate),c);
		c.insets = new Insets(0,5,5,0);
		c.gridx = 1;
		c.gridy = 0;
		this.add(getLabelRemainingTasks(remainingTasks),c);
		c.insets = new Insets(0,5,5,0);
		c.gridx = 2;
		c.gridy = 0;
		this.add(getIntTextFieldsolved(),c);		
		c.insets = new Insets(0,5,5,0);
		c.gridx = 3;
		c.gridy = 0;
		this.add(getDateTextField(),c);
		c.insets = new Insets(0,5,5,0);
		c.gridx = 4;
		c.gridy = 0;
		this.add(getButtonSolve(),c);	
	}
	
	private JLabel getLabelReceivedDate(String receivedDate){
		labelReceivedDateLabel = new JLabel(receivedDate,JLabel.LEFT);
		labelReceivedDateLabel.setPreferredSize(new Dimension(70,20));
		return labelReceivedDateLabel;
	}
	private JLabel getLabelRemainingTasks(String remainingTasks){
		labelRemainingTasks = new JLabel(remainingTasks,JLabel.LEFT);
		labelRemainingTasks.setPreferredSize(new Dimension(40,20));
		return labelRemainingTasks;
	}	
	
	private JLabel getDateTextField(){
		labelSolvedDate = new JLabel(now(),JLabel.CENTER);
		labelSolvedDate.setPreferredSize(new Dimension(70,20));
		labelSolvedDate.setBackground(new Color(255,255,255));
		labelSolvedDate.setOpaque(true);
		labelSolvedDate.setBorder(intTextFieldsolved.getBorder());
		labelSolvedDate.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				Calendar cal = new Calendar(labelSolvedDate.getText()+" 00:00:00:000");
				labelSolvedDate.setText(cal.getOracleDate());
			}
		});
		return labelSolvedDate;
	}
	
	private IntTextField getIntTextFieldsolved(){
		intTextFieldsolved.setPreferredSize(new Dimension(40,20));
		intTextFieldsolved.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {
			    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			    	solveTask();
		        }
			}
			public void keyReleased(KeyEvent e) {}
			public void keyTyped(KeyEvent e) {}
		});
		return intTextFieldsolved;
	}
	
	private void solveTask(){
		if(validateDate()& validateNumberOfTasks()){
		sOracle.solveTasks(parent.getTaskID(), getNoTask(),getAgent(),labelReceivedDateLabel.getText(),labelSolvedDate.getText());
		parent.updateTaskPanelPanel();
		}
	}
	
	private JButton getButtonSolve(){
		JButton b = new JButton("L�s");
		b.setPreferredSize(new Dimension(50,20));
		b.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				solveTask();
			}
		});
		b.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {
			    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			    	solveTask();
		        }
			}
			public void keyReleased(KeyEvent e) {}
			public void keyTyped(KeyEvent e) {}
		});
		return b;
	}

	private int getNoTask(){
		return Integer.parseInt("0"+intTextFieldsolved.getText());
	}
	
	public String getAgent(){
		return System.getenv("USERNAME").toUpperCase();
	}
	
	private String now() {
		java.util.Calendar cal = java.util.Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(cal.getTime());
	}
	
	private boolean validateDate(){
		 DateFormat formatter; 
		 java.util.Date solveDate; 
		 java.util.Date receivedDate; 
		
		try {	
		  formatter = new SimpleDateFormat("yyyy-MM-dd");
		  solveDate = (java.util.Date)formatter.parse(labelSolvedDate.getText());  
		  receivedDate = (java.util.Date)formatter.parse(labelReceivedDateLabel.getText());  
		  
		  
		  java.util.Calendar cal = java.util.Calendar.getInstance();
		  if (cal.getTime().compareTo(solveDate)<0){
			  
		  JOptionPane.showMessageDialog(parent,
					"Du kan ikke l�se opgaver ud i fremtiden",
				    "Fejl-40",
				    JOptionPane.ERROR_MESSAGE);
		  return false;
		  }
		  
		  if(cal.getTime().compareTo(solveDate)>0){
			  formatter = new SimpleDateFormat("yyyy-MM-dd");
			  solveDate = (java.util.Date)formatter.parse(labelSolvedDate.getText());  
			  
			  if (receivedDate.compareTo(solveDate)>0){
				  
			  JOptionPane.showMessageDialog(parent,
						"Du kan ikke l�se opgaver f�r de er modtaget",
					    "Fejl-40",
					    JOptionPane.ERROR_MESSAGE);
			  return false;  
		  }
		  }
		  } catch (ParseException e) {} 
		return true;
	}
	
	
	private boolean validateNumberOfTasks(){
		
		int remainingTasks = Integer.parseInt(labelRemainingTasks.getText());
		
		
		if (remainingTasks<getNoTask()){
			  JOptionPane.showMessageDialog(parent,
						"<html>Du kan ikke l�se flere opgaver end der er modtaget. <br>" +
						"�ndre tallet eller tilf�j flere opgaver f�rst",
					    "Fejl-40",
					    JOptionPane.ERROR_MESSAGE);
			  return false;  
			
		}
			
		
		
		return true;
	}
	
	
	
	
}
