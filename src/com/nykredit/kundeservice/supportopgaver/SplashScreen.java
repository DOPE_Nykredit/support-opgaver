package com.nykredit.kundeservice.supportopgaver;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.UIManager;
import com.nykredit.kundeservice.swing.NDialog;
import com.nykredit.kundeservice.supportopgaver.tasksolveframe.TaskSolveFrame;

public class SplashScreen extends NDialog {
	private static final long serialVersionUID = 1L;
	private static String program = "Support opgaver v1.5";
	private JLabel labelSplash = null;

	public static void main(String[] args) {
		try {
	        UIManager.setLookAndFeel(
	            UIManager.getSystemLookAndFeelClassName());
	    } catch (Exception e) {}
		
		SplashScreen splash = new SplashScreen();
		TaskSolveFrame main = new TaskSolveFrame(program);
		main.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		splash.dispose();
	}

	public SplashScreen(){
		this.setSize(400, 150);
		this.setLocationRelativeTo(this.getRootPane());
		this.setUndecorated(true);
		this.setContentPane(getLabelSplash());
		this.setTitle(program);
		this.setVisible(true);
	}
	 /* SPLASH */
	private JLabel getLabelSplash(){
		if (labelSplash == null){
			labelSplash = new JLabel(program,JLabel.CENTER);
			labelSplash.setOpaque(true);
			labelSplash.setBackground(new Color(34,56,127));
			labelSplash.setForeground(Color.white);
			labelSplash.setFont(new Font("Verdana", Font.ITALIC, 25));
		}
		return labelSplash;
	}
}