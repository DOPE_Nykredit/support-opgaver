package com.nykredit.kundeservice.supportopgaver.taskadministrationframe;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import com.nykredit.kundeservice.swing.NDialog;
import com.nykredit.kundeservice.supportopgaver.customarraylistobjects.TaskColumns;
import com.nykredit.kundeservice.supportopgaver.database.SOracle;
import com.nykredit.kundeservice.supportopgaver.tasksolveframe.TaskSolveFrame;



public class TaskAdministration extends NDialog{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TaskSolveFrame parent;
	private SOracle sOracle;
	private JTable taskListTable;
	private JComboBox<String>
	ComboCreatedDate = new JComboBox<String>(),
	ComboTaskName = new JComboBox<String>(),
	ComboCompletedBy = new JComboBox<String>(),
	ComboDeadline = new JComboBox<String>(),
	ComboCompletedDate = new JComboBox<String>(),
	ComboReceivedDate = new JComboBox<String>(),
	ComboDeleted = new JComboBox<String>(),
	ComboCreatedBy = new JComboBox<String>(),
	ComboActualCompletedDate = new JComboBox<String>(),
	ComboUpdatedDate = new JComboBox<String>(),
	ComboUpdatedBy = new JComboBox<String>();
	private JLabel selectedCount = new JLabel("",JLabel.LEFT);
	
	

	public TaskAdministration(TaskSolveFrame parent,SOracle sOracle){
		this.parent = parent;
		this.sOracle = sOracle;
		this.setTitle("Opgaveliste");
		this.setContentPane(getMainPane());
		this.pack();
		this.setResizable(false);
		this.setModal(true);
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	}
	
	
	private JPanel getButtonPanel(){
		JPanel p = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		c.insets = new Insets(0,0,0,5);
		c.gridx = 0;
		p.add(getUpdateButton(),c);	

		c.insets = new Insets(0,0,0,5);
		c.gridx = 1;
		p.add(getDeleteButton(),c);	
		
		c.insets = new Insets(0,0,0,0);
		c.gridx = 2;
		p.add(getUnDeleteButton(),c);

		return p;
	}
	
	
	private JPanel getComboPanel(){
		JPanel p = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		c.insets = new Insets(0,4,0,4);
		p.add(getCombo(ComboReceivedDate, sOracle.getUniqueReceivedDate(),"Modtaget dato"),c);
		p.add(getCombo(ComboTaskName, sOracle.getUniqueTask(),"Opgave"),c);
		p.add(getCombo(ComboCreatedDate, sOracle.getUniqueCreatedDate(),"Oprettet dato"),c);
		p.add(getCombo(ComboCreatedBy, sOracle.getUniqueCreatedBy(),"Oprettet af"),c);
		p.add(getCombo(ComboDeadline, sOracle.getUniqueDeadline(),"Deadline"),c);
		p.add(getCombo(ComboCompletedDate, sOracle.getUniqueCompletedDate(),"Afsluttet dato"),c);
		p.add(getCombo(ComboActualCompletedDate, sOracle.getUniqueActualCompletedDate(),"Faktisk afsluttet"),c);
		p.add(getCombo(ComboCompletedBy, sOracle.getUniqueCompletedBy(),"Afsluttet af"),c);
		p.add(getCombo(ComboDeleted, sOracle.getUniqueDeleted(),"Slettet"),c);
		p.add(getCombo(ComboUpdatedDate, sOracle.getUniqueUpdatedDate(),"Opdateret dato"),c);
		p.add(getCombo(ComboUpdatedBy, sOracle.getUniqueUpdatedBy(),"Opdateret af"),c);
		
		return p;
	}

	private JPanel getMainPane(){
		JPanel p = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		c.gridy = 0;
		c.insets = new Insets(10,10,5,0);
		c.anchor = GridBagConstraints.WEST;
		p.add(getButtonPanel(),c);
		c.gridy = 1;
		c.insets = new Insets(0,0,0,20);
		c.anchor = GridBagConstraints.EAST;
		p.add(getComboPanel(),c);
		c.insets = new Insets(0,0,0,0);
		c.gridy = 2;
		p.add(getTaskListTable(),c);
		c.anchor = GridBagConstraints.CENTER;
		c.gridy = 3;
		p.add(selectedCount,c);
		
		this.ComboReceivedDate.setSelectedIndex(1);
		
		updateTaskListTable();
		return p;
	}
	
	private JButton getDeleteButton(){
		JButton b = new JButton("Slet");
		b.setPreferredSize(new Dimension(90,40));
		b.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				for (int i:taskListTable.getSelectedRows()){
					sOracle.setDeleted(Integer.parseInt(taskListTable.getValueAt(i, 0).toString()));
				}
				updateTaskListTable();
			}
		});
		return b;
	}
	
	private JButton getUnDeleteButton(){
		JButton b = new JButton("<html>Annuller<br>sletning");
		b.setPreferredSize(new Dimension(90,40));
		b.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				for (int i:taskListTable.getSelectedRows()){
					sOracle.setUnDeleted(Integer.parseInt(taskListTable.getValueAt(i, 0).toString()));
				}
				updateTaskListTable();
			}
		});
		return b;
	}
	
	private JButton getUpdateButton(){
		JButton b = new JButton("<html>Opdat�r<br>oversigt");
		b.setPreferredSize(new Dimension(90,40));
		b.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				updateTaskListTable();
			}
		});
		return b;
	}
	
	private JScrollPane getTaskListTable(){
		DefaultTableModel m = getTaskListTableModel();
		taskListTable = new JTable(m);

		SelectionListener listener = new SelectionListener(taskListTable,selectedCount);
		taskListTable.getSelectionModel().addListSelectionListener(listener);
		taskListTable.getColumnModel().getSelectionModel().addListSelectionListener(listener);

		JScrollPane sp = new JScrollPane();
		sp.setViewportView(taskListTable);
		sp.setPreferredSize(new Dimension(1300,500));
		return sp;
	}
	
	private DefaultTableModel getTaskListTableModel(){
		DefaultTableModel m = new DefaultTableModel();
		m.addColumn("Id");
		m.addColumn("Modtaget dato");
		m.addColumn("Opgave");
		m.addColumn("Oprettet dato");
		m.addColumn("Oprettet af");
		m.addColumn("Deadline");
		m.addColumn("Afsluttet dato");
		m.addColumn("Faktisk afsluttet");
		m.addColumn("Afsluttet af");
		m.addColumn("Slettet");
		m.addColumn("Opdateret dato");
		m.addColumn("Opdateret af");
		return m;
	}
	
	private void updateTaskListTable(){
		DefaultTableModel m = getTaskListTableModel();
		taskListTable.setModel(m);
		
		ArrayList<TaskColumns> tc = sOracle.getTaskData(
				ComboCreatedDate.getSelectedItem().toString(),
				ComboCompletedBy.getSelectedItem().toString(),
				ComboDeadline.getSelectedItem().toString(),
				ComboCompletedDate.getSelectedItem().toString(),
				ComboReceivedDate.getSelectedItem().toString(),
				ComboCreatedBy.getSelectedItem().toString(),
				ComboActualCompletedDate.getSelectedItem().toString(),
				ComboUpdatedDate.getSelectedItem().toString(),
				ComboUpdatedBy.getSelectedItem().toString(),
				ComboTaskName.getSelectedItem().toString(),
				ComboDeleted.getSelectedItem().toString()
				);
		
		for (TaskColumns col:tc){
			
			m.addRow(new Object[] {
					col.getId(),
					col.getReceivedDate(),
					col.getTaskName(),
					col.getCreatedDate(),
					col.getCreatedBy(),
					col.getDeadline(),
					col.getCompletedDate(),
					col.getActualCompletedDate(),
					col.getCompletedBy(),
					col.getDeleted(),
					col.getUpdatedDate(),
					col.getUpdatedBy()
					});
		}

		selectedCount.setText(taskListTable.getSelectedRowCount()+" ud af "+taskListTable.getRowCount()+" poster valgt");
		parent.updatePanel();
	}
	

	private JPanel getCombo(JComboBox<String> combo, ResultSet rs,String header){
		JPanel p = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		combo.addItem("-- V�lg Alle --");
		combo.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				updateTaskListTable();
			}
		});
		try {
			while (rs.next()){
				combo.addItem(rs.getString(1));
			}
		} catch (SQLException e) {e.printStackTrace();}
		
		JLabel label = new JLabel(header,JLabel.LEFT); 
		label.setPreferredSize(new Dimension(99,20));
		combo.setPreferredSize(new Dimension(99,20));
		
		c.insets = new Insets(0,0,0,0);
		c.gridy = 0;
		p.add(label,c);
		c.gridy = 1;
		p.add(combo,c);

		return p;
	}

}
