package com.nykredit.kundeservice.supportopgaver.taskadministrationframe;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class SelectionListener implements ListSelectionListener {
    JTable table;
    JLabel selectedCount;

    SelectionListener(JTable table, JLabel selectedCount) {
        this.table = table;
        this.selectedCount = selectedCount;
    }
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource() == table.getSelectionModel()
              && table.getRowSelectionAllowed()) {
        } else if (e.getSource() == table.getColumnModel().getSelectionModel()
               && table.getColumnSelectionAllowed() ){
        }

        selectedCount.setText(table.getSelectedRowCount()+" ud af "+table.getRowCount()+" poster valgt");
        if (e.getValueIsAdjusting()) {
        }
    }
}

