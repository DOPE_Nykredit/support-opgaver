package com.nykredit.kundeservice.supportopgaver.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import com.nykredit.kundeservice.data.CTIRConnection;
import com.nykredit.kundeservice.supportopgaver.customarraylistobjects.Task;
import com.nykredit.kundeservice.supportopgaver.customarraylistobjects.TaskColumns;
import com.nykredit.kundeservice.supportopgaver.overviewframe.TableOverViewData;
import com.nykredit.kundeservice.supportopgaver.overviewframe.SpecifiedRowColumns;
import com.nykredit.kundeservice.supportopgaver.overviewframe.TableOverViewData2;
import com.nykredit.kundeservice.supportopgaver.overviewframe.TotalRowColumns;

public class SOracle extends CTIRConnection{
		
	public SOracle(String program){
		try {this.Connect();
		} catch (SQLException e) {e.printStackTrace();}
		this.SetupSpy(program);
		this.SS_spy();
	}
	
	public ResultSet getTaskList(int taskID){
		return getResultSetTable(
				"	SELECT														" +
				"	TO_CHAR(DAT.RECEIVED_DATE,'YYYY-MM-DD') AS RECEIVED_DATE,	" +
				"	TYP.NAME,													" +
				"	COUNT(DAT.CREATED_DATE) AS REMAING_TASKS					" +
				"FROM															" +
				"	KS_DRIFT.SOP_TASK_DATA DAT									" +
				"INNER JOIN														" +
				"	KS_DRIFT.SOP_TASK_TYPE TYP									" +
				"ON DAT.TASK_ID=TYP.ID											" +
				"WHERE 															" +
				"	DAT.COMPLETED_DATE IS NULL AND								" +
				"	DAT.DELETED = 0	AND											" +
				"	DAT.TASK_ID ="+taskID+"										" +
				"GROUP BY 														" +
				"	DAT.RECEIVED_DATE,											" +
				"	TYP.NAME													" +
				"ORDER BY DAT.RECEIVED_DATE 									");
	}

	public ArrayList<Task> getTaskList(){
		ArrayList<Task> tl = new ArrayList<Task>();
		try {
			ResultSet rs = Hent_tabel("SELECT ID,NAME,DEADLINE_IN_DAYS FROM KS_DRIFT.SOP_TASK_TYPE WHERE RANK>0 ORDER BY NAME");
			while (rs.next()){
				tl.add(new Task(rs.getInt("ID"),rs.getString("NAME"), rs.getInt("DEADLINE_IN_DAYS")));
			}
		} catch (SQLException e) {e.printStackTrace();}
		return tl;
	}
	
	
	public void addTask(int noTask,int taskid,String created_by,String received_date,String deadline){
	
		String sql = 
				"INSERT INTO 																										" +
				"KS_DRIFT.SOP_TASK_DATA (CREATED_DATE,CREATED_BY,TASK_ID,DEADLINE,RECEIVED_DATE,DELETED,UPDATED_DATE,UPDATED_BY)			" +
				"VALUES (sysdate,'"+created_by+"',"+taskid+",TO_DATE('"+deadline+"','YYYY-MM-DD'),'"+received_date+"',0,sysdate,'"+created_by+"')	";	

		for(int i = 1;i<=noTask;i++){
			try {
				this.Hent_intet(sql);
			} catch (SQLException e) {e.printStackTrace();}
		}
	}
	
	public void addAndSolveTask(int noTask,int taskid,String created_by,String received_date,String deadline){
		
		String sql = 
				"INSERT INTO " +
				"KS_DRIFT.SOP_TASK_DATA (CREATED_DATE,CREATED_BY,TASK_ID,COMPLETED_BY,DEADLINE,COMPLETED_DATE,RECEIVED_DATE,DELETED,UPDATED_DATE,UPDATED_BY,ACTUAL_COMPLETED_DATE)			" +
				"VALUES (sysdate,'"+created_by+"',"+taskid+",'"+created_by+"',TO_DATE('"+deadline+"','YYYY-MM-DD'),'"+received_date+"','"+received_date+"',0,sysdate,'"+created_by+"',sysdate)	";	

		for(int i = 1;i<=noTask;i++){
			try {
				this.Hent_intet(sql);
			} catch (SQLException e) {e.printStackTrace();}
		}
	}

	public void solveTasks(int taskID,int noTask, String completedBy,String receivedDate,String solvedDate){
		String sql = 
				"UPDATE " +
				"(" +
				"	SELECT" +
				"		*" +
				"	FROM" +
				"		KS_DRIFT.SOP_TASK_DATA" +
				"	WHERE " +
				"		COMPLETED_DATE IS NULL AND " +
				"		DELETED = 0 AND " +
				"		TASK_ID ="+taskID+" AND " +
				" 		RECEIVED_DATE='"+receivedDate+"' " +
				") " +
				" SET " +
				"	COMPLETED_BY='"+completedBy+"', " +
				"	ACTUAL_COMPLETED_DATE=TO_DATE('"+now()+"','YYYY-MM-DD HH24:MI:SS'),  " +
				"	UPDATED_BY='"+completedBy+"', " +
				"	UPDATED_DATE=TO_DATE('"+now()+"','YYYY-MM-DD HH24:MI:SS'),  " +
				"	COMPLETED_DATE=TO_DATE('"+solvedDate+"','YYYY-MM-DD')  " +
				"WHERE ROWNUM<="+noTask;
		
		try {
			this.Hent_intet(sql);
		} catch (SQLException e) {e.printStackTrace();}
		
	}
	
	private String now() {
		java.util.Calendar cal = java.util.Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(cal.getTime());
	}
	
	/**
	 * 
	 * @param dateReceived
	 * @param DeadlineInDays
	 * @return based on date received and the numbers of days a person has to solve a given task
	 * the function returns the deadline based on days the bank is open except for Sundays where this team
	 * does not work despite the fact the bank is open.
	 */
	public String getDeadline(String dateReceived,int deadlineInDays){
		try {
			return this.Hent_String(1,
				"SELECT 													" +
				"	DEADLINE 												" +
				"FROM 														" +
				"	( 														" +
				"	SELECT 													" +
				"		ROWNUM AS ROW_NUMBER, 								" +
				"		TO_CHAR(THIS_DATE,'YYYY-MM-DD') AS DEADLINE 		" +
				"	FROM 													" +
				"		(SELECT THIS_DATE  FROM KS_DRIFT.SYS_DATE_KS WHERE TO_CHAR(THIS_DATE,'DY') NOT IN ('L�','S�')) " +
				"	WHERE													" +
				"	    THIS_DATE>=TO_DATE('"+dateReceived+"','YYYY-MM-DD') " +
				"	ORDER BY												" +
				"		THIS_DATE 											" +
				"	) WHERE ROW_NUMBER ="+deadlineInDays+"+1				");
		} catch (SQLException e) {
			return "ERROR";
		}
	}
	
	public ArrayList<TaskColumns> getTaskData(
			String createdDate,
			String completedBy,
			String deadline,
			String completedDate,
			String receivedDate,
			String createdBy,
			String actualCompletedDate,
			String updatedDate,
			String updatedBy,
			String taskName,
			String deleted
			){
		
			String whereCriteria = "";
		
		
			if (!createdDate.contentEquals("-- V�lg Alle --"))
				whereCriteria =whereCriteria+"TRUNC(CREATED_DATE)='"+createdDate+"' AND ";
			if (!completedBy.contentEquals("-- V�lg Alle --"))
				whereCriteria =whereCriteria+"COMPLETED_BY='"+completedBy+"' AND ";
			if (!deadline.contentEquals("-- V�lg Alle --"))
				whereCriteria =whereCriteria+"DEADLINE='"+deadline+"' AND ";
			if (!completedDate.contentEquals("-- V�lg Alle --"))
				whereCriteria =whereCriteria+"COMPLETED_DATE='"+completedDate+"' AND ";
			if (!receivedDate.contentEquals("-- V�lg Alle --"))
				whereCriteria =whereCriteria+"RECEIVED_DATE='"+receivedDate+"' AND ";
			if (!createdBy.contentEquals("-- V�lg Alle --"))
				whereCriteria =whereCriteria+"CREATED_BY='"+createdBy+"' AND ";
			if (!actualCompletedDate.contentEquals("-- V�lg Alle --"))
				whereCriteria =whereCriteria+"TRUNC(ACTUAL_COMPLETED_DATE)='"+actualCompletedDate+"' AND ";
			if (!updatedDate.contentEquals("-- V�lg Alle --"))
				whereCriteria =whereCriteria+"TRUNC(UPDATED_DATE)='"+updatedDate+"' AND ";
			if (!updatedBy.contentEquals("-- V�lg Alle --"))
				whereCriteria =whereCriteria+"UPDATED_BY='"+updatedBy+"' AND ";
			if (!taskName.contentEquals("-- V�lg Alle --"))
				whereCriteria =whereCriteria+"TASK_ID=(SELECT ID FROM KS_DRIFT.SOP_TASK_TYPE WHERE NAME='"+taskName+"') AND ";
			if (!deleted.contentEquals("-- V�lg Alle --")){
				if (deleted.contentEquals("Ja"))
						whereCriteria =whereCriteria+"DELETED=1 AND ";
				if (deleted.contentEquals("Nej"))
					whereCriteria =whereCriteria+"DELETED=0 AND ";				
			}
				
		
		ArrayList<TaskColumns> al = new ArrayList<TaskColumns>();
		ResultSet rs = getResultSetTable(
				"SELECT																					" +
				"	DAT.ID AS ID,																		" +
				"	TO_CHAR(CREATED_DATE,'YYYY-MM-DD HH24:MI:SS') AS CREATED_DATE, 						" +
				"	COMPLETED_BY,																		" +
				"	TO_CHAR(DEADLINE,'YYYY-MM-DD') AS DEADLINE, 										" +
				"	TO_CHAR(COMPLETED_DATE,'YYYY-MM-DD') AS COMPLETED_DATE, 							" +
				"	TO_CHAR(RECEIVED_DATE,'YYYY-MM-DD') AS RECEIVED_DATE, 								" +
				"	CREATED_BY,																			" +
				"	TO_CHAR(ACTUAL_COMPLETED_DATE,'YYYY-MM-DD HH24:MI:SS') AS ACTUAL_COMPLETED_DATE, 	" +
				"	TO_CHAR(UPDATED_DATE,'YYYY-MM-DD HH24:MI:SS') AS UPDATED_DATE,						" +
				"	UPDATED_BY,																			" +
				"	NAME AS TASK_NAME,																	" +
				"	CASE WHEN DELETED = 1 THEN 'Ja' ELSE '' END AS DELETED								" +
				"FROM 																					" +
				"	KS_DRIFT.SOP_TASK_DATA DAT															" +
				"INNER JOIN 																			" +
				"	KS_DRIFT.SOP_TASK_TYPE TYP															" +
				"ON 																					" +
				"	DAT.TASK_ID=TYP.ID																	" +
				"WHERE 1=1 AND "+whereCriteria+"1=1														" +
				"ORDER BY 																				" +
				"	CREATED_DATE, 																		" +
				"	TASK_NAME, 																			" +
				"	CREATED_BY, 																		" +
				"	COMPLETED_DATE, 																	" +
				"	COMPLETED_BY 																		");
				
				
		try {
			while (rs.next()){
				al.add(new TaskColumns(
						rs.getInt("ID"),
						rs.getString("CREATED_DATE"),
						rs.getString("COMPLETED_BY"),
						rs.getString("DEADLINE"),
						rs.getString("COMPLETED_DATE"),
						rs.getString("RECEIVED_DATE"),
						rs.getString("CREATED_BY"),
						rs.getString("ACTUAL_COMPLETED_DATE"),
						rs.getString("UPDATED_DATE"),
						rs.getString("UPDATED_BY"),
						rs.getString("TASK_NAME"),
						rs.getString("DELETED")
						));
			}
		} catch (SQLException e) {e.printStackTrace();}
		return al;
	}

	private ResultSet getResultSetTable(String sql){
		try {
			return this.Hent_tabel(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void setDeleted(int id){
		
		try {
			this.Hent_intet("UPDATE KS_DRIFT.SOP_TASK_DATA SET DELETED=1,UPDATED_DATE=sysdate,UPDATED_BY='"+getAgent()+"' WHERE ID="+id+" AND DELETED=0");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setUnDeleted(int id){
		
		try {
			this.Hent_intet("UPDATE KS_DRIFT.SOP_TASK_DATA SET DELETED=0,UPDATED_DATE=sysdate,UPDATED_BY='"+getAgent()+"' WHERE ID="+id+" AND DELETED=1");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ResultSet getUniqueCreatedDate(){
		return getResultSetTable("SELECT DISTINCT TO_CHAR(CREATED_DATE,'YYYY-MM-DD') AS CREATED_DATE FROM KS_DRIFT.SOP_TASK_DATA ORDER BY 1 DESC");
	}

	public ResultSet getUniqueTask(){
		return getResultSetTable("SELECT DISTINCT NAME FROM KS_DRIFT.SOP_TASK_DATA DAT INNER JOIN KS_DRIFT.SOP_TASK_TYPE TYP ON TASK_ID=TYP.ID  ORDER BY 1");
	}
	
	public ResultSet getUniqueCompletedBy(){
		return getResultSetTable("SELECT DISTINCT COMPLETED_BY FROM KS_DRIFT.SOP_TASK_DATA WHERE COMPLETED_BY IS NOT NULL  ORDER BY 1");
	}
	
	public ResultSet getUniqueDeadline(){
		return getResultSetTable("SELECT DISTINCT TO_CHAR(DEADLINE,'YYYY-MM-DD') AS DEADLINE FROM KS_DRIFT.SOP_TASK_DATA  ORDER BY 1 DESC");
	}
	
	public ResultSet getUniqueCompletedDate(){
		return getResultSetTable("SELECT DISTINCT TO_CHAR(COMPLETED_DATE,'YYYY-MM-DD') AS COMPLETED_DATE FROM KS_DRIFT.SOP_TASK_DATA WHERE COMPLETED_DATE IS NOT NULL  ORDER BY 1 DESC");
	}
	
	public ResultSet getUniqueReceivedDate(){
		return getResultSetTable("SELECT DISTINCT TO_CHAR(RECEIVED_DATE,'YYYY-MM-DD') AS RECEIVED_DATE FROM KS_DRIFT.SOP_TASK_DATA  ORDER BY 1 DESC");
	}
	
	public ResultSet getUniqueDeleted(){
		return getResultSetTable("SELECT 'Ja' AS DELETED FROM DUAL UNION ALL SELECT 'Nej' AS DELETED FROM DUAL ORDER BY 1");
	}
	
	public ResultSet getUniqueCreatedBy(){
		return getResultSetTable("SELECT DISTINCT CREATED_BY FROM KS_DRIFT.SOP_TASK_DATA ORDER BY 1");
	}
	
	public ResultSet getUniqueActualCompletedDate(){
		return getResultSetTable("SELECT DISTINCT TO_CHAR(ACTUAL_COMPLETED_DATE,'YYYY-MM-DD') AS ACTUAL_COMPLETED_DATE FROM KS_DRIFT.SOP_TASK_DATA WHERE ACTUAL_COMPLETED_DATE IS NOT NULL ORDER BY 1 DESC");
	}
	
	public ResultSet getUniqueUpdatedDate(){
		return getResultSetTable("SELECT DISTINCT TO_CHAR(UPDATED_DATE,'YYYY-MM-DD') AS UPDATED_DATE FROM KS_DRIFT.SOP_TASK_DATA ORDER BY 1 DESC");
	}
	
	public ResultSet getUniqueUpdatedBy(){
		return getResultSetTable("SELECT DISTINCT UPDATED_BY FROM KS_DRIFT.SOP_TASK_DATA ORDER BY 1");
	}
	
	private String getAgent(){
		return System.getenv("USERNAME").toUpperCase();
	}
	
	public ResultSet getTaskType(){
		return this.getResultSetTable(
			"SELECT ID, KPI, NAME,BGR(COLOR) AS COLOR,DEADLINE_DESCRIPTION,DEADLINE_IN_DAYS, EST_SEC_TO_COMPLETE FROM KS_DRIFT.SOP_TASK_TYPE WHERE RANK<>0 ORDER BY RANK"
				);
	}
	
	public String getResponsible(int taskID,String date){
		try {
			return Hent_String(1, "SELECT RESPONSIBLE FROM KS_DRIFT.SOP_RESPONSIBLE WHERE R_DATE='"+date+"' AND TASK_ID="+taskID);
		} catch (SQLException e) {return null;}	
	}
	
	public ResultSet getResponsibleCategory(String date){
		return this.getResultSetTable(
				"SELECT TASK_ID, RESPONSIBLE FROM KS_DRIFT.SOP_RESPONSIBLE WHERE R_DATE='"+date+"' AND RECEIVED_DATE='1983-01-27'"
				);
	}
	
	public ResultSet getUnsolvedReceivedSolved(String date){
		return getResultSetTable(
				"SELECT 'UNSOLVED' AS TYPE, TASK_ID, COUNT(ID) AS COUNT FROM KS_DRIFT.SOP_TASK_DATA WHERE COMPLETED_DATE IS NULL AND DELETED=0 GROUP BY TASK_ID 	" +
				"UNION ALL																																			" +
				"SELECT 'RECEIVED' AS TYPE,TASK_ID, COUNT(ID) AS COUNT FROM KS_DRIFT.SOP_TASK_DATA WHERE RECEIVED_DATE='"+date+"' AND DELETED=0 GROUP BY TASK_ID	" + 
				"UNION ALL																																			" +
				"SELECT 'SOLVED' AS TYPE, TASK_ID, COUNT(ID) AS COUNT FROM KS_DRIFT.SOP_TASK_DATA WHERE COMPLETED_DATE='"+date+"' AND DELETED=0 GROUP BY TASK_ID	");
	}
	
	public void setResponsible(String date,int taskID,String responsible, String receivedDate){
		
		if (responsible.contentEquals("")){
			try {
				this.Hent_intet("DELETE KS_DRIFT.SOP_RESPONSIBLE WHERE R_DATE='"+date+"' AND TASK_ID="+taskID +" AND RECEIVED_DATE='"+receivedDate+"'");
			} catch (SQLException e) {e.printStackTrace();}
		} else {
			try {
				this.Hent_intet("INSERT INTO KS_DRIFT.SOP_RESPONSIBLE (R_DATE,TASK_ID,RESPONSIBLE,RECEIVED_DATE) VALUES ('"+date+"',"+taskID+",'"+responsible+"','"+receivedDate+"')");
			} catch (SQLException e) {
				e.printStackTrace();
				
				try {
					this.Hent_intet("UPDATE KS_DRIFT.SOP_RESPONSIBLE SET RESPONSIBLE='"+responsible+"' WHERE R_DATE='"+date+"' AND TASK_ID="+taskID+" AND RECEIVED_DATE='"+receivedDate+"'");
				} catch (SQLException e1) {e1.printStackTrace();}
			}
		}
	}
	
public void removeResponsible(String date,int taskID,String receivedDate){
			try {
				this.Hent_intet("DELETE KS_DRIFT.SOP_RESPONSIBLE WHERE R_DATE='"+date+"' AND TASK_ID="+taskID+" AND RECEIVED_DATE='"+receivedDate+"'");
			} catch (SQLException e) {}
	}

public ArrayList<SpecifiedRowColumns> getLeftovers(String date){
	ResultSet rs = this.getResultSetTable(
			"SELECT 																										" +
			"	DAT.TASK_ID,																								" +
			"	TO_CHAR(DAT.RECEIVED_DATE,'YYYY-MM-DD') AS RECEIVED_DATE,													" +
			"	COUNT(DAT.RECEIVED_DATE) AS LEFTOVER,																		" +
			"	RES.RESPONSIBLE,																							" +
			"	CASE WHEN DAT.DEADLINE=TRUNC(SYSDATE) THEN 1 WHEN DAT.DEADLINE<TRUNC(SYSDATE) THEN 2 ELSE 0 END AS PAINT	" +
			"FROM 																											" +
			"	KS_DRIFT.SOP_TASK_DATA DAT																					" +
			"LEFT OUTER JOIN 																								" +
			"	(SELECT * FROM KS_DRIFT.SOP_RESPONSIBLE WHERE R_DATE='"+date+"') RES										" +
			"ON 																											" +
			"	DAT.RECEIVED_DATE=RES.RECEIVED_DATE AND 																	" +
			"	DAT.TASK_ID=RES.TASK_ID 																					" +
			"WHERE 																											" +
			"	DELETED=0 AND 																								" +
			"	COMPLETED_DATE IS NULL	 																					" +
			"GROUP BY																										" +
			"	DAT.TASK_ID,																								" +
			"	DAT.RECEIVED_DATE,																							" +
			"	RES.RESPONSIBLE, 																							" +
			"	RES.RECEIVED_DATE,																							" +
			"	DAT.DEADLINE																								" +
			"ORDER BY 																										" +
			"	DAT.RECEIVED_DATE 																							");
	
	ArrayList<SpecifiedRowColumns> al = new ArrayList<SpecifiedRowColumns>();
	
	try {
		while (rs.next()) {

			al.add(new SpecifiedRowColumns(rs.getInt("TASK_ID"), rs
					.getString("RECEIVED_DATE"), rs.getInt("LEFTOVER"), rs
					.getString("RESPONSIBLE"), rs.getInt("PAINT")));
		}
	} catch (SQLException e) {e.printStackTrace();}

	return al;
}

public ArrayList<TotalRowColumns> getTotalRowColumns(String date){
	
	ArrayList<TotalRowColumns> al = new ArrayList<TotalRowColumns>();

	ResultSet rs = null;
	rs = getTaskType();

		try {
			while (rs.next()) {
				al.add(new TotalRowColumns(rs.getString("NAME"),rs.getString("KPI"),  rs
						.getString("DEADLINE_DESCRIPTION"), rs.getInt("ID"), rs
						.getInt("COLOR"), rs.getInt("DEADLINE_IN_DAYS"), rs.getInt("EST_SEC_TO_COMPLETE")));

			}

		rs = getUnsolvedReceivedSolved(date);
		while (rs.next()) {
			for (TotalRowColumns col : al) {
				if (col.getTaskID() == rs.getInt("TASK_ID")
						& rs.getString("TYPE").contentEquals("UNSOLVED"))
					col.setUnSolved(rs.getInt("COUNT"));
				if (col.getTaskID() == rs.getInt("TASK_ID")
						& rs.getString("TYPE").contentEquals("SOLVED"))
					col.setSolved(rs.getInt("COUNT"));
				if (col.getTaskID() == rs.getInt("TASK_ID")
						& rs.getString("TYPE").contentEquals("RECEIVED"))
					col.setReceived(rs.getInt("COUNT"));
			}
		}

		rs = getResponsibleCategory(date);
		while (rs.next()) {
			for (TotalRowColumns col : al) {
				if (col.getTaskID() == rs.getInt("TASK_ID"))
					col.setResponsible(rs.getString("RESPONSIBLE"));
			}
		}

		} catch (SQLException e) {e.printStackTrace();}
		
		return al;
}

	public ResultSet getTotal(String whereClause,String startDate, String endDate){
				
		String str = 
			"SELECT 																									" +
			"	NAME, 																									" +
			"	SUM(SOLVED)AS SOLVED, 																				    " +
			"	SUM(EXCEEDED_DEADLINE) AS EXCEEDED_DEADLINE ,															" +
			"	((SUM(SOLVED) - SUM(EXCEEDED_DEADLINE))/SUM(SOLVED)) AS PROCENTLOST, 								    " +	
			"	SUM(EXCEEDED_DEADLINE)/SUM(SOLVED) AS FELTERIALT											    		" +
			"FROM 																										" +
			"	( 																										" +
			"	SELECT 																									" + 
			"	TYP.NAME AS NAME, 																						" +
			"	COUNT(AA.COMPLETED_DATE) AS SOLVED, 																	" +
			"	CASE WHEN AA.COMPLETED_DATE>DEADLINE THEN COUNT(AA.COMPLETED_DATE) ELSE 0 END AS EXCEEDED_DEADLINE 	" +
			"	FROM KS_DRIFT.SOP_TASK_DATA AA INNER JOIN SOP_TASK_TYPE TYP ON AA.TASK_ID=TYP.ID "+WHERE(whereClause)+" " +
			"	WHERE AA.COMPLETED_DATE>='"+startDate+"' AND AA.COMPLETED_DATE<='"+endDate+"' AND DELETED=0 			" +
			"	GROUP BY TYP.NAME,AA.DEADLINE,AA.COMPLETED_DATE  														" +
			"	) 																										" +
			"GROUP BY 																									" + 
			"	NAME																									" +
			"ORDER BY 1 																								";
		
		
		ResultSet rs = null;
		try {rs = this.Hent_tabel(str);} catch (SQLException e) {e.printStackTrace();}
		return rs;
	}
	
	private String WHERE(String whereClause) {
		return "INNER JOIN KS_DRIFT.V_TEAM_DATO VT							\n"+
		"ON AA.COMPLETED_BY = VT.INITIALER									\n"+
		"AND AA.COMPLETED_DATE = VT.DATO									\n"+	
		"INNER JOIN KS_DRIFT.AGENTER_TEAMS AT								\n"+
		"ON VT.TEAM = AT.TEAM " + whereClause;
	}

	public TableOverViewData getRestOverView() throws SQLException {
		TableOverViewData rest = new TableOverViewData();
		
		/*
		 * ikke deadline
		 */
		ResultSet rs = this.getResultSetTable(" SELECT COUNT(NAME) as notDeadLine FROM KS_DRIFT.SOP_TASK_DATA DAT INNER " +
				"JOIN KS_DRIFT.SOP_TASK_TYPE TYP ON TYP.ID=DAT.TASK_ID WHERE DAT.COMPLETED_DATE IS NULL AND " +
				"TRUNC(SYSDATE)<DAT.DEADLINE AND DAT.DELETED=0 AND DAT.TASK_ID!=75");	
		try {
			while (rs.next()) {		
				rest.setNotDeadlineToday(rs.getInt("notDeadLine"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		 * Deadline
		 */
		rest.setDeadlineToday(getRestDeadline());
		
		/*
		 * overskredet
		 */
		rest.setExceeded(getRestExeeced());

		return rest;
	}
	private int getRestExeeced() {
		ResultSet rs3 = this.getResultSetTable("SELECT COUNT(DAT.ID) AS exceeded " +
				"FROM KS_DRIFT.SOP_TASK_DATA DAT " +
				"INNER JOIN KS_DRIFT.SOP_TASK_TYPE TYP " +
				"ON TYP.ID=DAT.TASK_ID " +
				"WHERE ((DAT.COMPLETED_DATE > DAT.DEADLINE) OR (DAT.COMPLETED_DATE IS NULL AND TRUNC(SYSDATE)>DAT.DEADLINE )) " +
				"AND DAT.DELETED=0 AND (TRUNC(SYSDATE)-RECEIVED_DATE>DEADLINE_IN_DAYS AND DAT.COMPLETED_DATE IS NULL) ");
	
		try {
			rs3.next();
			return rs3.getInt("exceeded");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	private int getRestDeadline() {
		ResultSet rs2 = this.getResultSetTable(
				"SELECT COUNT(NAME) as deadline FROM KS_DRIFT.SOP_TASK_DATA " +
				"DAT INNER JOIN KS_DRIFT.SOP_TASK_TYPE TYP ON TYP.ID=DAT.TASK_ID " +
				"WHERE DAT.COMPLETED_DATE IS NULL AND (TRUNC(SYSDATE)=DAT.DEADLINE-TYP.DEADLINE_IN_DAYS " +
				"AND TRUNC(SYSDATE)=DAT.RECEIVED_DATE+TYP.DEADLINE_IN_DAYS OR TRUNC(SYSDATE)=DAT.RECEIVED_DATE " +
				"AND TYP.DEADLINE_IN_DAYS=0 OR TRUNC(SYSDATE)=DAT.DEADLINE)  AND DAT.DELETED=0"
				
				);

		try {
			rs2.next();
			return rs2.getInt("deadline");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	public TableOverViewData2 getSolvedOverView(){
		TableOverViewData2 solved = new TableOverViewData2();
		
		/*
		 * ikke deadline
		 */
		ResultSet rs4 = this.getResultSetTable(" SELECT COUNT(TYP.NAME) AS notDeadline2 " +
				"FROM KS_DRIFT.SOP_TASK_DATA DAT " +
				"INNER JOIN KS_DRIFT.SOP_TASK_TYPE TYP " +
				"ON TYP.ID=DAT.TASK_ID " +
				"WHERE DAT.COMPLETED_DATE IS NOT NULL " +
				"AND TRUNC(SYSDATE)<DAT.DEADLINE AND DAT.COMPLETED_DATE=TRUNC(SYSDATE) AND DAT.DELETED=0 " );
		
		try {
			while (rs4.next()) {		
				solved.setNotDeadlineToday2(rs4.getInt("notDeadLine2"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		 * Deadline
		 */
		solved.setDeadlineToday2(getSolvedDeadline2());
		
		/*
		 * overskredet
		 */
		solved.setExceeded2(getSolvedExeeced2());

		return solved;
	}
	private int getSolvedExeeced2() {
		ResultSet rs5 = this.getResultSetTable("SELECT COUNT(NAME) as exceeded2 " +
				"FROM KS_DRIFT.SOP_TASK_DATA DAT " +
				"INNER JOIN KS_DRIFT.SOP_TASK_TYPE TYP " +
				"ON TYP.ID=DAT.TASK_ID WHERE ((DAT.COMPLETED_DATE > DAT.DEADLINE) " +
				"OR (DAT.COMPLETED_DATE IS NULL AND TRUNC(SYSDATE)>DAT.DEADLINE )) " +
				"AND DAT.DELETED=0 AND (TRUNC(SYSDATE)-RECEIVED_DATE>DEADLINE_IN_DAYS " +
				"AND DAT.COMPLETED_DATE=TRUNC(SYSDATE))");
	
		try {
			rs5.next();
			return rs5.getInt("exceeded2");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	private int getSolvedDeadline2() {
		ResultSet rs6 = this.getResultSetTable(
				"SELECT COUNT(TYP.NAME) AS deadline2 FROM KS_DRIFT.SOP_TASK_DATA DAT INNER JOIN KS_DRIFT.SOP_TASK_TYPE TYP ON TYP.ID=DAT.TASK_ID WHERE DAT.COMPLETED_DATE IS NOT NULL AND TRUNC(SYSDATE)=DAT.COMPLETED_DATE AND (TRUNC(SYSDATE)=DAT.DEADLINE-TYP.DEADLINE_IN_DAYS AND TRUNC(SYSDATE)=DAT.RECEIVED_DATE+TYP.DEADLINE_IN_DAYS OR TRUNC(SYSDATE)=DAT.RECEIVED_DATE AND TYP.DEADLINE_IN_DAYS=0 OR TRUNC(SYSDATE)=DAT.DEADLINE)  AND DAT.DELETED=0"
				
				);

		try {
			rs6.next();
			return rs6.getInt("deadline2");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
		
}

