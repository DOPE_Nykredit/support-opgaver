package com.nykredit.kundeservice.supportopgaver.overviewframe;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import com.nykredit.kundeservice.swing.Calendar;
import com.nykredit.kundeservice.util.Date;
import com.nykredit.kundeservice.swing.NFrame;
import com.nykredit.kundeservice.supportopgaver.database.SOracle;
import com.nykredit.kundeservice.supportopgaver.tasksolveframe.TaskSolveFrame;



public class Overview extends NFrame implements WindowListener {

	private static final long serialVersionUID = 1L;
	private JLabel dateLabel;
	private Date date = new Date();
	private SOracle sOracle;
	private JPanel rowPanel;
	private JScrollPane scrollRowPanel;
	private JTable taskOverviewTable;
	
private JPanel p = new JPanel(new GridBagLayout());
	public Overview(SOracle sOracle, TaskSolveFrame parent) {
		WindowListener[] test = this.getWindowListeners();
		for (WindowListener list : test) {
			this.removeWindowListener(list);
		}
		this.addWindowListener(this);
		this.sOracle = sOracle;
		this.setTitle("Oversigt");
		this.setMinimumSize(new Dimension(1000, 800));
		this.setLocationRelativeTo(parent);
		this.setContentPane(getMainPanel());
		updateRows();
		this.setVisible(true);
	}

	private JPanel getMainPanel() {
	
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(5, 0, 0, 0);
		c.gridx = 0;
		c.gridy = 0;
		c.fill = GridBagConstraints.BOTH;
		c.anchor = GridBagConstraints.CENTER;
		p.add(getDatePanel(), c);

		c.insets = new Insets(0, 0, 0, 0);
		c.gridy = 1;
		c.weighty = 0.02; 
		c.ipady = 10;
		p.add(getLegendPanel(), c);	

		c.weighty = 0;
		c.ipady=0;
		c.fill = GridBagConstraints.BOTH;
		c.gridy = 2;
		c.anchor = GridBagConstraints.CENTER;
		p.add(getHeaderPanel(), c);

		c.fill = GridBagConstraints.BOTH;
		c.weightx=1;
		c.weighty = 1;
		c.gridy = 3;
		c.anchor = GridBagConstraints.WEST;
		p.add(getRowPanelPane(), c);
		
		return p;
	}
	
	private JScrollPane getRowPanelPane() {
		scrollRowPanel = new JScrollPane();
		//scrollRowPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollRowPanel.setSize(800, 800);
		scrollRowPanel.setViewportView(getRowPanel());
		return scrollRowPanel;
	}

	private JPanel getRowPanel() {
		rowPanel = new JPanel(new GridBagLayout());
		rowPanel.setBackground(Color.white);
		updateRows();
		return rowPanel;
	}
	
	public void updateRows() {
		rowPanel.removeAll();
		GridBagConstraints c = new GridBagConstraints();

		ArrayList<TotalRowColumns> al = sOracle.getTotalRowColumns(dateLabel.getText());
		ArrayList<SpecifiedRowColumns> specRowCol = sOracle.getLeftovers(dateLabel.getText());
		
		c.gridx = 0;
		c.anchor = GridBagConstraints.WEST;
		c.fill = GridBagConstraints.NONE;
		
		int i = 0;
		for (TotalRowColumns col : al) {
			c.gridy = i;
			rowPanel.add(
					new TotalRowPanel(col.getTaskID(),
									dateLabel.getText(),
									sOracle, Overview.this,
									col.getTaskName(), 
									col.getKpi(),
									col.getDeadlineDescription(),
									col.getUnSolved(),
									col.getReceived(),
									col.getColor(),
									col.getSolved(),
									col.getResponsible(),
									col.getEstimat(),
									specRowCol), c);
			i++;
		};

		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1;
		c.weighty = 1;
		c.gridy = i;
		rowPanel.add(new JLabel(), c);
		Overview.this.repaint();
		Overview.this.validate();		
	}

	private JPanel getLegendPanel() { 
		
		JPanel p = new JPanel();
		     
		taskOverviewTable = new JTable(getTaskOverviewData());  
		JScrollPane pane = new JScrollPane(taskOverviewTable); 
	
		MyRenderer mr = new MyRenderer();
		mr.setHorizontalAlignment(JLabel.CENTER);
		mr.setColor(new Color(155,255,140));
		taskOverviewTable.getColumnModel().getColumn(1).setPreferredWidth(100);
		taskOverviewTable.getColumnModel().getColumn(1).setCellRenderer(mr);
		
		MyRenderer mr2 = new MyRenderer();
		mr2.setHorizontalAlignment(JLabel.CENTER);
		mr2.setColor(new Color(255,255,140));		
		taskOverviewTable.getColumnModel().getColumn(2).setCellRenderer(mr2);
		
		MyRenderer mr3 = new MyRenderer();
		mr3.setHorizontalAlignment(JLabel.CENTER);
		mr3.setColor(new Color(255,155,140));
		taskOverviewTable.getColumnModel().getColumn(3).setCellRenderer(mr3);
		taskOverviewTable.setEnabled(false);
		
		MyRenderer mr4 = new MyRenderer();
		mr4.setHorizontalAlignment(JLabel.CENTER);
		mr4.setColor(Color.white);
		taskOverviewTable.getColumnModel().getColumn(4).setCellRenderer(mr4);
		taskOverviewTable.setEnabled(false);
		
		p.setSize(150, 140);
		p.add(pane);
		return p;
	}



	private DefaultTableModel getTaskOverviewData(){
		ArrayList<TableOverViewData> taskesOverviewList  = new ArrayList<TableOverViewData>();
		try {
			taskesOverviewList.add(sOracle.getRestOverView());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ArrayList<TableOverViewData2> taskesOverviewList2  = new ArrayList<TableOverViewData2>();
		taskesOverviewList2.add(sOracle.getSolvedOverView());
	
		Object[][] data = new Object[][]{{"Rest",taskesOverviewList.get(0).getNotDeadlineToday(),taskesOverviewList.get(0).getDeadlineToday(),taskesOverviewList.get(0).getExceeded(),taskesOverviewList.get(0).getDeadlineToday()+taskesOverviewList.get(0).getExceeded()},   
							  {"L�st",taskesOverviewList2.get(0).getNotDeadlineToday2(),taskesOverviewList2.get(0).getDeadlineToday2(),taskesOverviewList2.get(0).getExceeded2(),taskesOverviewList2.get(0).getDeadlineToday2()+taskesOverviewList2.get(0).getExceeded2()}};
		
		String col[] = {"","Ikke deadline i dag","Deadline i dag","Overskredet","Tal til DOPE"};  
		DefaultTableModel model = new DefaultTableModel(data, col);
		
		return model;
	}
	
	private JPanel getDatePanel() {
		JPanel p = new JPanel(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(0, 0, 0, 0);
		c.gridx = 0;
		c.gridy = 0;
		p.add(getBackButton(), c);

		c.insets = new Insets(0, 2, 0, 0);
		c.gridx = 1;
		c.gridy = 0;
		p.add(getDateLabel(), c);

		c.insets = new Insets(0, 2, 0, 0);
		c.gridx = 2;
		c.gridy = 0;
		p.add(getForwardButton(), c);

		c.insets = new Insets(0, 2, 0, 0);
		c.gridx = 3;
		c.gridy = 0;
		p.add(getUpdateButton(), c);

		return p;
	}

	private JButton getUpdateButton() {
		JButton b = new JButton("Opdat�r");
		b.setPreferredSize(new Dimension(100, 20));
		b.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				updateRows();				
				Overview.this.taskOverviewTable.setModel(Overview.this.getTaskOverviewData());
				
				MyRenderer mr = new MyRenderer();
				mr.setHorizontalAlignment(JLabel.CENTER);
				mr.setColor(new Color(155,255,140));
				taskOverviewTable.getColumnModel().getColumn(1).setPreferredWidth(100);
				taskOverviewTable.getColumnModel().getColumn(1).setCellRenderer(mr);
				
				MyRenderer mr2 = new MyRenderer();
				mr2.setHorizontalAlignment(JLabel.CENTER);
				mr2.setColor(new Color(255,255,140));		
				taskOverviewTable.getColumnModel().getColumn(2).setCellRenderer(mr2);
				
				MyRenderer mr3 = new MyRenderer();
				mr3.setHorizontalAlignment(JLabel.CENTER);
				mr3.setColor(new Color(255,155,140));
				taskOverviewTable.getColumnModel().getColumn(3).setCellRenderer(mr3);
				taskOverviewTable.setEnabled(false);
				
				MyRenderer mr4 = new MyRenderer();
				mr4.setHorizontalAlignment(JLabel.CENTER);
				mr4.setColor(Color.white);
				taskOverviewTable.getColumnModel().getColumn(4).setCellRenderer(mr4);
				taskOverviewTable.setEnabled(false);
			}
		});
		b.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					updateRows();
				}
			}

			public void keyReleased(KeyEvent e) {
			}

			public void keyTyped(KeyEvent e) {
			}
		});
		return b;
	}

	private JButton getBackButton() {
		JButton b = new JButton("<< Tilbage");
		b.setPreferredSize(new Dimension(100, 20));
		b.setMinimumSize(new Dimension(100, 20));
		b.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				dateLabel.setText(date.datoadd(dateLabel.getText(), -1));
				updateRows();
			}
		});
		return b;
	}

	private JButton getForwardButton() {
		JButton b = new JButton("Frem >>");
		b.setPreferredSize(new Dimension(100, 20));
		b.setMinimumSize(new Dimension(100, 20));
		b.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				dateLabel.setText(date.datoadd(dateLabel.getText(), 1));
				updateRows();
			}
		});
		return b;
	}

	private JLabel getDateLabel() {
		dateLabel = new JLabel(now(), JLabel.CENTER);
		dateLabel.setPreferredSize(new Dimension(70, 20));
		dateLabel.setBorder(BorderFactory.createLineBorder(Color.black));
		dateLabel.setBackground(Color.white);
		dateLabel.setOpaque(true);
		dateLabel.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				Calendar cal = new Calendar(dateLabel.getText()
						+ " 00:00:00:000");
				dateLabel.setText(cal.getOracleDate());
				updateRows();
			}
		});
		return dateLabel;
	}

	private String now() {
		java.util.Calendar cal = java.util.Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(cal.getTime());
	}

	private JLabel getLabelHeader(String text) {
		JLabel l = new JLabel(text, JLabel.LEFT);
		return l;
	}

	private JPanel getHeaderPanel() {
		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));

		p.add(getLabelHeader(" Opgave"));
		p.add(Box.createHorizontalStrut(285));
		p.add(getLabelHeader(" KPI"));
		p.add(Box.createHorizontalStrut(5));
		p.add(getLabelHeader(" Deadline"));
		p.add(Box.createHorizontalStrut(105));
		p.add(getLabelHeader(" Rest"));
		p.add(Box.createHorizontalStrut(25));
		p.add(getLabelHeader(" L�st"));
		p.add(Box.createHorizontalStrut(25));
		p.add(getLabelHeader(" Modtaget"));
		p.add(Box.createHorizontalStrut(4));
		p.add(getLabelHeader(" Ansvarlig(e)"));
		p.add(Box.createHorizontalStrut(227));
		p.add(getLabelHeader("Estimat"));
		p.add(Box.createHorizontalStrut(25));

		return p;
	}

	public void windowActivated(WindowEvent arg0) {}
	public void windowClosed(WindowEvent arg0) {}
	public void windowClosing(WindowEvent arg0) {this.dispose();}
	public void windowDeactivated(WindowEvent arg0) {}
	public void windowDeiconified(WindowEvent arg0) {}
	public void windowIconified(WindowEvent arg0) {}
	public void windowOpened(WindowEvent arg0) {}

}
