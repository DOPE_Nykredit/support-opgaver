package com.nykredit.kundeservice.supportopgaver.overviewframe;

public class SpecifiedRowColumns {
	
	private int taskID;
	private String receivedDate;
	private int leftover;
	private String responsible;
	private int paint;
		
	public SpecifiedRowColumns(int taskID,String receivedDate,int leftover,String responsible, int paint){
		this.taskID = taskID;
		this.receivedDate = receivedDate;
		this.leftover = leftover;
		this.responsible = responsible;
		this.paint = paint;
	}

	public int getTaskID() {return taskID;}
	public void setTaskID(int taskID) {this.taskID = taskID;}

	public String getReceivedDate() {return receivedDate;}
	public void setReceivedDate(String receivedDate) {this.receivedDate = receivedDate;}

	public int getLeftover() {return leftover;}
	public void setLeftover(int leftover) {this.leftover = leftover;}

	public String getResponsible() {return responsible;}
	public void setResponsible(String responsible) {this.responsible = responsible;}

	public int getPaint() {return paint;}
	public void setPaint(int paint) {this.paint = paint;}
}
