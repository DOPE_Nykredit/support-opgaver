package com.nykredit.kundeservice.supportopgaver.overviewframe;

public class TableOverViewData {

	private int notDeadline;
	private int deadlineToday;
	private int exceeded;
	
	public int getNotDeadlineToday() {
		return notDeadline;
	}
	public void setNotDeadlineToday(int notDeadlineToday) {
		this.notDeadline = notDeadlineToday;
	}
	public int getDeadlineToday() {
		return deadlineToday;
	}
	public void setDeadlineToday(int deadlineToday) {
		this.deadlineToday = deadlineToday;
	}
	public int getExceeded() {
		return exceeded;
	}
	public void setExceeded(int exceeded) {
		this.exceeded = exceeded;
	}
}
