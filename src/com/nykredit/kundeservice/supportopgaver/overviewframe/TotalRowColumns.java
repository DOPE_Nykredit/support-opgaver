package com.nykredit.kundeservice.supportopgaver.overviewframe;

public class TotalRowColumns {
        
        private String
        kpi,
        taskName,
        deadlineDescription,
        responsible;
        private int 
        taskID,
        color,
        deadlineInDays,
        received,
        unsolved,
        estimat,
        solved;
        
        public TotalRowColumns( 
                        String taskName,
                        String kpi,
                        String deadlineDescription,
                        int taskID,
                        int color,
                        int deadlineInDays,
                        int estimat){
        		this.taskName = taskName;
        		this.kpi=kpi;
                this.deadlineDescription = deadlineDescription;
                this.taskID = taskID;
                this.color = color;
                this.deadlineInDays = deadlineInDays;
                this.estimat = estimat;
        }

        public String getTaskName() {return taskName;}
        public void setTaskName(String taskName) {this.taskName = taskName;}

        public String getDeadlineDescription() {return deadlineDescription;}
        public void setDeadlineDescription(String deadlineDescription) {this.deadlineDescription = deadlineDescription;}

        public int getTaskID() {return taskID;}
        public void setTaskID(int taskID) {this.taskID = taskID;}
        
        public int getColor() {return color;}
        public void setColor(int color) {this.color = color;}
        
        public int getDeadlineInDays() {return deadlineInDays;}
        public void setDeadlineInDays(int deadlineInDays) {this.deadlineInDays = deadlineInDays;}

        public int getReceived() {return received;}
        public void setReceived(int received) {this.received = received;}

        public int getUnSolved() {return unsolved;}
        public void setUnSolved(int solved) {this.unsolved = solved;}

        public int getSolved() {return solved;}
        public void setSolved(int solved) {this.solved = solved;}

        public String getResponsible() {return this.responsible;}
        public void setResponsible(String responsible) {this.responsible = responsible;}

        public int getEstimat() {return estimat;}

        public String getKpi() {return this.kpi;}
        public void setKpi(String kpi) {this.kpi = kpi;}

 
		
}      
        
