package com.nykredit.kundeservice.supportopgaver.overviewframe;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.nykredit.kundeservice.supportopgaver.database.SOracle;

public class TotalRowPanel extends JPanel{

        private static final long serialVersionUID = 1L;
        private JLabel labelTask;
        private JLabel labelRemainingTasks;
        private JLabel labelSolved;
        private JLabel responsibleLabel;
        private Overview parent;
        private SOracle sOracle;
        private String date;
		private int taskID;
		private int estimat;
		private JLabel labelkpi;
		private String kpi;

        public TotalRowPanel(
                        int taskID,
                        String date,
                        SOracle sOracle,
                        Overview parent,
                        String taskName,
                        String kpi,
                        String deadlineDescription,
                        int rest,
                        int modtaget,
                        int colorInt,
                        int solved,
                        String responsible,
                        int estimat,
                        ArrayList<SpecifiedRowColumns> al
                        ){
                this.taskID = taskID;
                this.date = date;
                this.sOracle = sOracle;
                this.parent = parent;
                Color color = new Color(colorInt);
                this.setBackground(Color.white);
                this.setLayout(new GridBagLayout());
                
                GridBagConstraints c = new GridBagConstraints();
                
                c.gridy = 0;
                c.gridx = 0;
                c.anchor = GridBagConstraints.NORTHWEST;
                this.add(getTaskName(taskName,color),c);
                
                c.gridx = 1;
                this.add(GetKpi(kpi, color),c);
                
                c.gridx = 2;
                this.add(getDeadlineDescription(deadlineDescription),c);
                
                c.gridx = 3;
                this.add(getRest(rest),c);
                
                c.gridx = 4;
                this.add(getSolved(solved),c);
                
                c.gridx = 5;
                this.add(getModtaget(modtaget),c);  
                
                c.gridx = 6; 
                this.add(getResponsible(responsible, taskID, rest, modtaget),c);
                                               
                c.gridx = 7;
                this.add(getEstimat(estimat),c);
                               
           
                //2nd row
                
                int y=1;

                        for (SpecifiedRowColumns col: al){
                                
                                if (col.getTaskID() == taskID){
                                        c.gridy = y;
                                        c.gridx = 0;
                                        c.gridwidth = 8;
                                        c.anchor = GridBagConstraints.WEST;
                                        this.add(
                                                        new SpecifiedRowPanel(
                                                        "Modtaget :",
                                                        taskID,
                                                        date,
                                                        col.getReceivedDate(),
                                                        sOracle,
                                                        parent,
                                                        col.getResponsible(),
                                                        col.getLeftover(),
                                                        col.getPaint()
                                                        ),c
                                                        );
                                        
                                        y++;
                                }
                                
                        }
        }

		private JLabel getTaskName(String taskName,Color color){
                Color textColor = Color.white;
                int orientation = JLabel.LEFT;
                if (color.equals(Color.white)){
                        textColor = Color.black;
                        orientation = JLabel.RIGHT;
                }
                labelTask = new JLabel(" "+taskName,orientation);
                labelTask.setPreferredSize(new Dimension(325,15));
                labelTask.setBackground(color);

                
                labelTask.setForeground(textColor);
                
                labelTask.setOpaque(true);
                return labelTask;
        }
        
        private JLabel getResponsible(String responsible, final int taskID, int rest, int modtaget ){
                responsibleLabel = new JLabel(responsible,JLabel.LEFT);
                responsibleLabel.setPreferredSize(new Dimension(285,15));
                System.out.println(responsible);
                responsibleLabel.setBackground(Color.red);
                if(taskID == 1 && responsible == null  || taskID == 7 && responsible == null || taskID == 8  && responsible == null || taskID == 21  && responsible == null || taskID == 31 && responsible == null || taskID == 51 && responsible == null || taskID == 54 && responsible == null || taskID == 56 && responsible == null || taskID == 57 && responsible == null || taskID == 64 && responsible == null ||taskID == 65 && responsible == null || taskID == 66 && responsible == null      || taskID == 67 && responsible == null  || taskID == 74 && responsible == null || taskID == 85 && responsible == null || taskID == 94 && responsible == null || taskID == 92 && responsible == null || taskID == 91 && responsible == null  ||taskID == 32 && responsible == null)
                        responsibleLabel.setOpaque(true);
                responsibleLabel.addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseReleased(java.awt.event.MouseEvent e) {
                                new AddResponsibleFrame(parent,sOracle, "1983-01-27", taskID, responsibleLabel.getText(),date);
                        }
                });
                return responsibleLabel;
        }
        
        private JLabel getDeadlineDescription(String deadlineDescription){
                labelRemainingTasks = new JLabel(" "+deadlineDescription,JLabel.LEFT);
                labelRemainingTasks.setPreferredSize(new Dimension(150,15));
                return labelRemainingTasks;
        }       
        
        private JLabel getRest(int rest){
                
                String restStr = rest+"";
                if (rest == 0)
                        restStr = "";
                
                labelRemainingTasks = new JLabel(restStr,JLabel.LEFT);
                labelRemainingTasks.setPreferredSize(new Dimension(50,15));
                return labelRemainingTasks;
        }
        
        private JLabel getSolved(int solved){
                
                String solvedStr = solved+"";
                if (solved == 0)
                        solvedStr = "";
                
                labelSolved = new JLabel(solvedStr,JLabel.LEFT);
                labelSolved.setPreferredSize(new Dimension(50,15));
                return labelSolved;
        }
        
        private JLabel getModtaget(int modtaget){
                String modtagetStr = modtaget+"";
                if (modtaget == 0){
                        modtagetStr = "";
                }
                labelRemainingTasks = new JLabel(modtagetStr,JLabel.LEFT);
                labelRemainingTasks.setPreferredSize(new Dimension(50,15));
                return labelRemainingTasks;
        }          
        
        private JLabel getEstimat(int estimat){
            labelRemainingTasks = new JLabel(" "+estimat,JLabel.LEFT);
            labelRemainingTasks.setPreferredSize(new Dimension(30,15));
            return labelRemainingTasks;
    }

		public int getEstimat() {
			return estimat;
		}

		public void setEstimat(int estimat) {
			this.estimat = estimat;
		}
                   
		private JLabel GetKpi(String kpi,Color color ){
            Color textColor = Color.white;
            int orientation = JLabel.LEFT;
            if (color.equals(Color.white)){
                    textColor = Color.black;
                    orientation = JLabel.RIGHT;
            }
            labelkpi = new JLabel(" "+kpi,orientation);
            labelkpi.setPreferredSize(new Dimension(25,15));
            labelkpi.setBackground(color);

            
            labelkpi.setForeground(textColor);
            
            labelkpi.setOpaque(true);
            return labelkpi;
    }
		
}