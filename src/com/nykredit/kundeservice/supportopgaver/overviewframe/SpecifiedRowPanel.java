package com.nykredit.kundeservice.supportopgaver.overviewframe;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.nykredit.kundeservice.supportopgaver.database.SOracle;

public class SpecifiedRowPanel extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel labelRemainingTasks;
	private JLabel responsibleLabel;
	private JLabel receivedDateLabel;
	private Overview parent;
	private SOracle sOracle;
	private String receivedDate;
	private String date;
	private int taskID;

	public SpecifiedRowPanel(
			String legend,
			int taskID,
			String date,
			String receivedDate,
			SOracle sOracle,
			Overview parent,
			String responsible,
			int rest,
			int paint
			){
		this.date = date;
		this.taskID = taskID;
		this.receivedDate = receivedDate;
		this.sOracle = sOracle;
		
		if (paint ==2){
		this.setBackground(new Color(255,155,140));
		}else if (paint ==1) {
			this.setBackground(new Color(255,255,140));
		}
		else {
			this.setBackground(new Color(155,255,140));
		}
		
		this.parent = parent;
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
			
			JLabel header = new JLabel(legend,JLabel.RIGHT);
			header.setPreferredSize(new Dimension(350,15)); ///Modtaget: Dato
			header.setBackground(Color.white);
			header.setOpaque(true);

			c.anchor = GridBagConstraints.WEST;
			c.fill = GridBagConstraints.BOTH;
			c.gridy = 0;
			c.gridx = 0;
			this.add(header,c);	
			
			c.gridx = 1;
			this.add(getReceived(receivedDate),c);	
			
			c.gridx = 2;
			this.add(getRest(rest),c);	
		
			c.insets = new Insets(0,320,0,0);
			c.gridx = 3;
			this.add(getResponsible(responsible),c);
			
			

	}

	private JLabel getResponsible(final String responsible){
		responsibleLabel = new JLabel(responsible,JLabel.LEFT);
		responsibleLabel.setPreferredSize(new Dimension(100,15));
		
		responsibleLabel.addMouseListener(new java.awt.event.MouseAdapter() {
			  public void mouseReleased(MouseEvent e) {
			        new AddResponsibleFrame(SpecifiedRowPanel.this.parent, SpecifiedRowPanel.this.sOracle, SpecifiedRowPanel.this.receivedDate, SpecifiedRowPanel.this.taskID, SpecifiedRowPanel.this.responsibleLabel.getText(), SpecifiedRowPanel.this.date);
			      }
			    });
		return responsibleLabel;
	}
	
	private JLabel getRest(int rest){
		labelRemainingTasks = new JLabel(rest+"",JLabel.LEFT);
		labelRemainingTasks.setPreferredSize(new Dimension(50,15));
		return labelRemainingTasks;
	}
	
	private JLabel getReceived(String received){
		receivedDateLabel = new JLabel(received,JLabel.LEFT);
		receivedDateLabel.setPreferredSize(new Dimension(150,15));
		return receivedDateLabel;
	}
	
}
