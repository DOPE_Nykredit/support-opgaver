package com.nykredit.kundeservice.supportopgaver.overviewframe;

public class TableOverViewData2 {
	private int notDeadline2;
	private int deadlineToday2;
	private int exceeded2;
	
	public int getNotDeadlineToday2() {
		return notDeadline2;
	}
	public void setNotDeadlineToday2(int notDeadlineToday2) {
		this.notDeadline2=notDeadlineToday2;
	}
	public int getDeadlineToday2() {
		return deadlineToday2;
	}
	public void setDeadlineToday2(int deadlineToday2) {
		this.deadlineToday2 = deadlineToday2;
	}
	public int getExceeded2() {
		return exceeded2;
	}
	public void setExceeded2(int exceeded2) {
		this.exceeded2 = exceeded2;
	}
}

