package com.nykredit.kundeservice.supportopgaver.overviewframe;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

class MyRenderer extends DefaultTableCellRenderer  {  
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Color color;
	@Override
	public Component getTableCellRendererComponent(JTable table,
			Object value, boolean isSelected, boolean hasFocus, int row,
			int column) {
		DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();   
		dtcr.setHorizontalTextPosition(DefaultTableCellRenderer.CENTER);  
		setBackground(color);

		

		super.getTableCellRendererComponent(table, value, isSelected, hasFocus,
				row, column);
		return this;
	}
	public void setColor(Color color){
		this.color = color;
	}


} 
