package com.nykredit.kundeservice.supportopgaver.overviewframe;

import com.nykredit.kundeservice.supportopgaver.database.SOracle;
import com.nykredit.kundeservice.swing.NDialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AddResponsibleFrame extends NDialog
{
  private static final long serialVersionUID = 1L;
  private String date;
  private SOracle sOracle;
  private String currentResponsible;
  private String receivedDate;
  private int taskID;
  private JTextField responsibleTextField;
  private Overview parent;

  public AddResponsibleFrame(Overview parent, SOracle sOracle, String receivedDate, int taskID, String currentResponsible, String date)
  {
    this.sOracle = sOracle;
    this.date = date;
    this.parent = parent;
    this.taskID = taskID;
    this.receivedDate = receivedDate;
    this.currentResponsible = currentResponsible;
    setModal(true);
    setTitle("Tilf�j Ansvarlig");
    setContentPane(getMainPane());
    pack();
    setLocationRelativeTo(parent);
    setVisible(true);
  }

  private JPanel getMainPane()
  {
    JPanel p = new JPanel();
    GridBagConstraints c = new GridBagConstraints();

    c.insets = new Insets(0, 10, 5, 0);
    c.gridx = 0;
    c.gridy = 0;
    p.add(getTextfield(), c);

    c.insets = new Insets(0, 10, 5, 0);
    c.gridx = 1;
    c.gridy = 0;
    p.add(getRemoveButton(), c);

    c.insets = new Insets(0, 10, 5, 0);
    c.gridx = 2;
    c.gridy = 0;
    p.add(getSubmitButton(), c);

    return p;
  }

  private JTextField getTextfield() {
    this.responsibleTextField = new JTextField();
    this.responsibleTextField.setPreferredSize(new Dimension(200, 20));
    this.responsibleTextField.setText(this.currentResponsible);
    return this.responsibleTextField;
  }

  private JButton getSubmitButton() {
    JButton b = new JButton("Tilf�j");
    b.setPreferredSize(new Dimension(100, 20));
    b.addMouseListener(new MouseAdapter() {
      public void mouseReleased(MouseEvent e) {
        AddResponsibleFrame.this.sOracle.setResponsible(AddResponsibleFrame.this.date, AddResponsibleFrame.this.taskID, AddResponsibleFrame.this.responsibleTextField.getText(), AddResponsibleFrame.this.receivedDate);
        AddResponsibleFrame.this.dispose();
        AddResponsibleFrame.this.parent.updateRows();
      }
    });
    return b;
  }

  private JButton getRemoveButton() {
    JButton b = new JButton("Fjern");
    b.setPreferredSize(new Dimension(100, 20));
    b.addMouseListener(new MouseAdapter() {
      public void mouseReleased(MouseEvent e) {
        AddResponsibleFrame.this.sOracle.removeResponsible(AddResponsibleFrame.this.date, AddResponsibleFrame.this.taskID, AddResponsibleFrame.this.receivedDate);
        AddResponsibleFrame.this.dispose();
        AddResponsibleFrame.this.parent.updateRows();
      }
    });
    return b;
  }
}