package com.nykredit.kundeservice.supportopgaver.customarraylistobjects;

/**
 * 
 * @author crpe
 * @description
 * This class is used to store TaskID and TaskName for each task
 */
public class Task{
	private int taskID;
	private String taskName;
	private String kpi;
	private int deadline;
	
	public Task(int taskID,String taskName,int deadline){
		this.taskID = taskID;
		this.taskName = taskName;
		this.deadline = deadline;
	}
	public int getTaskID() {return taskID;}
	void setTaskID(int taskID) {this.taskID = taskID;}
	
	public String getTaskName() {return taskName;}
	public void setTaskName(String taskName) {this.taskName = taskName;}

	public int getDeadline() {return deadline;}
	public void setDeadline(int deadline) {this.deadline = deadline;}

	public String toString(){return taskName;}

}


