package com.nykredit.kundeservice.supportopgaver.customarraylistobjects;

public class TaskColumns {
	
	
	private String
	createdDate,
	completedBy,
	deadline,
	completedDate,
	receivedDate,
	createdBy,
	actualCompletedDate,
	updatedDate,
	updatedBy,
	taskName,
	deleted;
	private int id;
	
	
	public TaskColumns(
			int id,
			String createdDate,
			String completedBy,
			String deadline,
			String completedDate,
			String receivedDate,
			String createdBy,
			String actualCompletedDate,
			String updatedDate,
			String updatedBy,
			String taskName,
			String deleted
			){
		this.id = id;
		this.createdDate = createdDate;
		this.completedBy = completedBy;
		this.deadline = deadline;
		this.completedDate = completedDate;
		this.receivedDate = receivedDate;
		this.createdBy = createdBy;
		this.actualCompletedDate = actualCompletedDate;
		this.updatedDate = updatedDate;
		this.updatedBy = updatedBy;
		this.taskName = taskName;
		this.deleted = deleted;
		
	}
	
	public int getId() {return id;}
	public void setId(int id) {this.id = id;}

	public String getCreatedDate() {return createdDate;}
	public void setCreatedDate(String createdDate) {this.createdDate = createdDate;}

	public String getCompletedBy() {return completedBy;}
	public void setCompletedBy(String completedBy) {this.completedBy = completedBy;}

	public String getDeadline() {return deadline;}
	public void setDeadline(String deadline) {this.deadline = deadline;}

	public String getCompletedDate() {return completedDate;}
	public void setCompletedDate(String completedDate) {this.completedDate = completedDate;}

	public String getReceivedDate() {return receivedDate;}
	public void setReceivedDate(String receivedDate) {this.receivedDate = receivedDate;}

	public String getCreatedBy() {return createdBy;}
	public void setCreatedBy(String createdBy) {this.createdBy = createdBy;}

	public String getActualCompletedDate() {return actualCompletedDate;}
	public void setActualCompletedDate(String actualCompletedDate) {this.actualCompletedDate = actualCompletedDate;}

	public String getUpdatedDate() {return updatedDate;}
	public void setUpdatedDate(String updatedDate) {this.updatedDate = updatedDate;}

	public String getUpdatedBy() {return updatedBy;}
	public void setUpdatedBy(String updatedBy) {this.updatedBy = updatedBy;}
	
	public String getTaskName() {return taskName;}
	public void setTaskName(String taskName) {this.taskName = taskName;}

	public String getDeleted() {return deleted;}
	public void setDeleted(String deleted) {this.deleted = deleted;}





}
